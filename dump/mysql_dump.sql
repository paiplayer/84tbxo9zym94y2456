
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kits
-- ----------------------------
DROP TABLE IF EXISTS `kits`;
CREATE TABLE `kits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kit_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kit_price` decimal(10, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kits
-- ----------------------------
INSERT INTO `kits` VALUES (1, 'BEMACASH VESTUARIO NFC-e + IMPRESSORA + GAVETA', 259.90);
INSERT INTO `kits` VALUES (2, 'BEMACASH COMERCIO NFC-e + IMPRESSORA + GAVETA + LEITOR', 339.90);

-- ----------------------------
-- Table structure for kits_products
-- ----------------------------
DROP TABLE IF EXISTS `kits_products`;
CREATE TABLE `kits_products`  (
  `kit_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_count` int(11) NOT NULL,
  INDEX `fk_conp_kit`(`kit_id`) USING BTREE,
  INDEX `fk_conp_prod`(`product_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of kits_products
-- ----------------------------
INSERT INTO `kits_products` VALUES (1, 5, 1);
INSERT INTO `kits_products` VALUES (1, 2, 1);
INSERT INTO `kits_products` VALUES (1, 3, 1);
INSERT INTO `kits_products` VALUES (1, 6, 1);
INSERT INTO `kits_products` VALUES (1, 7, 1);
INSERT INTO `kits_products` VALUES (1, 4, 1);
INSERT INTO `kits_products` VALUES (2, 2, 1);
INSERT INTO `kits_products` VALUES (2, 3, 1);
INSERT INTO `kits_products` VALUES (2, 4, 1);
INSERT INTO `kits_products` VALUES (2, 6, 1);
INSERT INTO `kits_products` VALUES (2, 8, 1);
INSERT INTO `kits_products` VALUES (2, 1, 1);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` int(11) NOT NULL DEFAULT 1,
  `order_total_price` decimal(10, 2) NOT NULL,
  `order_licence` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_date` datetime(0) NULL DEFAULT NULL,
  `order_client_cnpj` varchar(18) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_client_phone` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_zip_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_address_state` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_address_country` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_comment` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `order_seller_executive` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_hardware_number` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_nfe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_nfe_date` datetime(0) NULL DEFAULT NULL,
  `order_kit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 3, 259.90, 'BEMACASH VESTUÁRIO NFCe', '2019-10-04 18:55:55', '13.459.202/0001-48', '(51) 99293-7660', 'Rua Santiago Dantas, 540', '91710-030', 'Rio Grande do Sul', 'Brasil', 'Sem comentários.', 'Aníbal Duarte Bersagui de Oliveira', 'G1SR6HH4S8H4DS6HH', '168181686168168', '2019-10-05 18:56:55', 1);
INSERT INTO `orders` VALUES (2, 5, 339.90, 'BEMACASH COMERCIO NFC-e', '2019-10-06 08:55:55', '12.345.678/0001-90', '(51) 3333-4444', 'Avenida Borges de Medeiros, 1234', '90000-000', 'Rio Grande do Sul', 'Brasil', 'Entregas não devem ser feitas nas quintas-feiras.', 'João Souza da Silva', 'V54YM9X4254,X4YM40', '7863974567601', '2019-10-06 18:56:55', 2);

-- ----------------------------
-- Table structure for orders_log
-- ----------------------------
DROP TABLE IF EXISTS `orders_log`;
CREATE TABLE `orders_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_log_date` datetime(0) NOT NULL,
  `order_log_content` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders_log
-- ----------------------------
INSERT INTO `orders_log` VALUES (1, 1, '2019-10-04 18:55:55', 'Pedido Iniciado');
INSERT INTO `orders_log` VALUES (2, 1, '2019-10-05 18:56:55', 'NFCe 168181686168168 emitita.');

-- ----------------------------
-- Table structure for orders_status
-- ----------------------------
DROP TABLE IF EXISTS `orders_status`;
CREATE TABLE `orders_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `style` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders_status
-- ----------------------------
INSERT INTO `orders_status` VALUES (1, 'Novo', 'secondary');
INSERT INTO `orders_status` VALUES (2, 'Aguardando Pagamento', 'info');
INSERT INTO `orders_status` VALUES (3, 'Pagamento Confirmado', 'success');
INSERT INTO `orders_status` VALUES (4, 'Faturado', 'warning');
INSERT INTO `orders_status` VALUES (5, 'Entregue', 'primary');
INSERT INTO `orders_status` VALUES (6, 'Cancelado', 'danger');
INSERT INTO `orders_status` VALUES (7, 'Vencido', 'dark');
INSERT INTO `orders_status` VALUES (8, 'Inválido', 'light');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, '1.jpg', 'Licença Software Bemacash');
INSERT INTO `products` VALUES (2, '2.jpg', 'TABLET SAMSUNG GALAXY TAB');
INSERT INTO `products` VALUES (3, '3.jpg', 'SUPORTE METALICO TABLET BEMACASH');
INSERT INTO `products` VALUES (4, '4.jpg', 'MP-4200 TH ETHERNET BR');
INSERT INTO `products` VALUES (5, '5.jpg', 'Licença Bemacash Vestuário');
INSERT INTO `products` VALUES (6, '6.jpg', 'GAVETA DINHEIRO GD-56 PRETA');
INSERT INTO `products` VALUES (7, '7.jpg', 'Licença de Software Fiscal Manager');
INSERT INTO `products` VALUES (8, '8.jpg', 'LEITOR CCD BT SCANNER BR-200BT');



ALTER TABLE `kits_products` ADD CONSTRAINT `fk_kit_comp` FOREIGN KEY (`kit_id`) REFERENCES `kits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `kits_products` ADD CONSTRAINT `fk_prod_comp` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `orders_log` ADD CONSTRAINT `fk_log_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `orders` ADD CONSTRAINT `fk_order_status` FOREIGN KEY (`order_status`) REFERENCES `orders_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `orders` ADD CONSTRAINT `fk_order_kit` FOREIGN KEY (`order_kit_id`) REFERENCES `kits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS = 1;
