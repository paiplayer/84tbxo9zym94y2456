<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	
    function __construct(){
		parent::__construct();
		$this->load->model('Orders_Model','ORDERS');
		$this->load->model('Products_Model','PRODUCTS');
	}
	
	// PÁGINA PRINCIPAL
	public function index(){
		$data['orders'] = $this->ORDERS->getAll();
		$data['kits'] = $this->PRODUCTS->getAllKits();
		$data['order_status'] = $this->ORDERS->getOrderStatus();

		$this->load->view('main',$data);
	}

	// OUTPUT PARA O AJAX DE DETALHES DE PEDIDO
	public function orderDetails($id){
		$data = $this->ORDERS->getDetails($id);
		$data['order_status'] = $this->ORDERS->getOrderStatus();
		$data['order_history'] = $this->ORDERS->getHistory($id);
		$this->load->view('order_details',$data);
	}

	// OUTPUT PARA O AJAX DE LISTA DE KITS DETALHADA
	public function kitsDetails(){
		$data['kits'] = $this->PRODUCTS->getDetailedKits();
		$this->load->view('kits_details',$data);
	}

}
