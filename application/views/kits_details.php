<?php
defined('BASEPATH') OR exit('No direct script access allowed');

foreach($kits AS $kit){ ?>
<table class="table">
    <tr class="alert alert-info">
        <th colspan="3" class="text-center"><?=$kit['kit_name']?></th>
    </tr>
    <?php foreach($kit['products'] AS $product){ ?>
    <tr>
        <td>
            <img src="/assets/images/<?=$product['image']?>" class="product-image">
        </td>
        <td>
            <?=$product['name']?>
        </td>
        <td>
            x<?=$product['product_count']?>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <th colspan="3" class="text-right">Valor do Kit: R$ <?=number_format($kit['kit_price'],2,',','.')?></th>
    </tr>
</table>
<hr><hr>
<?php } ?>