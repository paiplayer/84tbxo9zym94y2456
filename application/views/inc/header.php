<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Teste Totvs</title>
	<link rel="stylesheet" href="./assets/css/bundle.css">
	<link rel="stylesheet" href="./assets/css/styles.css">
  	<script src="./assets/js/bundle.js"></script>
</head>
<body class="body">