<?php require_once 'inc/header.php'; ?>
	<section class="container">
		<div class="row">
			<div class="col-lg-12">
				<button type="button" id="allKits" class="btn btn-primary pull-right" data-toggle="modal" data-target="#kitsModal">
					Ver Todos os Kits
				</button>
				<h2>Sistema de Visualização de Pedidos</h2>
			</div>
		</div>
		<div class="row" id="orderList">
			<?php foreach($orders AS $order){ ?>
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<button class="btn btn-primary order-details" type="button" data-toggle="collapse" data-id="<?=$order['id']?>" data-target="#collapse<?=$order['id']?>" aria-expanded="false" aria-controls="collapse<?=$order['id']?>">+</button>
						Pedido <?=$order['id']?> - <?=$kits[$order['order_kit_id']]['kit_name']?> -  <?=$order_status[$order['order_status']]['title']?> - <?=date('d\/m\/Y',strtotime($order['order_date']))?>
					</div>
				</div>
			</div>
			<div class="collapse col-lg-12" id="collapse<?=$order['id']?>"  data-parent="#orderList">
				<div class="card">
					<div id="cardBody<?=$order['id']?>">
						<center><img src="/assets/images/loading.gif" class="loading-image"><br><b>Carregando...</b></center>
					</div>
				</div>
			</div>
		<?php } ?>
		
		</div>
	</section>
	
	<div class="modal fade" id="kitsModal" tabindex="-1" role="dialog" aria-labelledby="kitsModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="kitsModalLabel">Título do modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="allKitsHolder">
				<center><img src="/assets/images/loading.gif" class="loading-image"><br><b>Carregando...</b></center>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
	</div>
</body>
<?php require_once 'inc/footer.php'; ?>