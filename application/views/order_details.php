<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
    <div class="col-lg-12 ">
        <div class="card-header alert-success text-center">
            <h3>Produtos do Pedido</h3>
        </div>
        <table class="table">
            <tr>
                <th colspan="3">Tipo de Licença: <?=$order['order_licence']?></th>
            </tr>
            <?php foreach($kit['products'] AS $product){ ?>
            <tr>
                <td>
                    <img src="/assets/images/<?=$product['image']?>" class="product-image">
                </td>
                <td>
                    <?=$product['name']?>
                </td>
                <td>
                    x<?=$product['product_count']?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div class="col-lg-12">
        <div class="card-header alert-info text-center">
            <h3>Detalhes do Pedido</h3>
        </div>
        <table class="table table-stripped">            
            <tr>
                <td>Status:</td>
                <td><span class="badge badge-<?=$order_status[$order['order_status']]['style']?>"><?=$order_status[$order['order_status']]['title']?></span></td>
            </tr>            
            <tr>
                <td>Preço</td>
                <td><b>R$ <?=number_format($order['order_total_price'],2,',','.')?></b></td>
            </tr>            
            <tr>
                <td>Data de Criação</td>
                <td><?=date('d\/m\/Y',strtotime($order['order_date']))?></td>
            </tr>
            <tr>
                <td>CNPJ para envio</td>
                <td><?=$order['order_client_cnpj']?></td>
            </tr>
            <tr>
                <td>Endereço para Entrega</td>
                <td><?=$order['order_address']?></td>
            </tr>
            <tr>
                <td>CEP da Entrega</td>
                <td><?=$order['order_zip_code']?></td>
            </tr>
            <tr>
                <td>Estado / País</td>
                <td><?=$order['order_address_state']?> / <?=$order['order_address_country']?></td>
            </tr>
            <tr>
                <td>Telefone do Cliente</td>
                <td><?=$order['order_client_phone']?></td>
            </tr>
            <tr>
                <td>Comentários Adicionais</td>
                <td><?=$order['order_comment']?></td>
            </tr>
            <tr>
                <td>Executivo de Vendas</td>
                <td><?=$order['order_seller_executive']?></td>
            </tr>
            <tr>
                <td>Número do Pedido de Hardware</td>
                <td><?=$order['order_hardware_number']?></td>
            </tr>
            <tr>
                <td>Número da NFE</td>
                <td><?=$order['order_nfe']?></td>
            </tr>
            <tr>
                <td>Data de Emissão da NFE</td>
                <td><?=date('d\/m\/Y',strtotime($order['order_nfe_date']))?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-12">
        <div class="card-header alert-dark text-center">
            <h3>Histórico do Pedido</h3>
        </div>
        <?php 
        if(count($order_history)>0){
        ?>
        <table class="table table-stripped">
            <tr>
                <th>Data e Hora</th>
                <th>Histórico</th>
            </tr>
            <?php 
                foreach($order_history AS $history){
            ?>
            <tr>
                <td><?=date('d\/m\/Y',strtotime($history['order_log_date']))?></td>
                <td><?=$history['order_log_content']?></td>
            </tr>
            <?php
                }
            } else {
            ?>
            <h4>Não existem registros de histórico para este pedido.</h4>
            <?php
            }
            ?>
        </table>
    </div>
</div>