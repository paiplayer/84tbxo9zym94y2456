<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_Model extends CI_Model {
    
    function __construct(){
		  parent::__construct();
    }
    
    // RETORNO DE DADOS BÁSICOS DE TODOS OS PEDIDOS PARA A LISTAGEM NA TELA PRINCIPAL
    public function getAll(){
        return $this->db  ->select('id,order_kit_id,order_status,order_date,order_licence')
                          ->get('orders')
                          ->result_array();
    }

    // RETORNO DE DETALHES DE UM PEDIDO EM ESPECÍFICO
    public function getDetails($id){
        $this->load->model('Products_Model','PRODUCTS');
        $result['order'] = $this->db  ->where('id',$id)
                                    ->get('orders')
                                    ->row_array();
        $result['kit'] = $this->PRODUCTS->getKitData($result['order']['order_kit_id']);
        return $result;
    }

    // LISTAGEM DE TIPOS DE STATUS DE PEDIDOS PARA TRADUÇÃO DOS COMPONENTES NO FRONTEND
    public function getOrderStatus(){
      $status = array();
      foreach($this->db->get('orders_status')->result_array() AS $info){
        $status[$info['id']] = $info;
      }
      return $status;
    }

    // RETORNO DO HISTÓRICO DE LOGS DE UM PEDIDO EM ESPECÍFICO
    public function getHistory($id){
      return $this->db  ->where('order_id',$id)
                        ->get('orders_log')
                        ->result_array();
    }
	

}