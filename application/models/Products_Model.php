<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_Model extends CI_Model {
    
    function __construct(){
		  parent::__construct();
    }
    
    // RETORNO DE ARRAY COM TODOS OS KITS CADASTRADOS 
    public function getAllKits(){
      $kits = array();
      foreach($this->db->get('kits')->result_array() AS $kit){
        $kits[$kit['id']] = $kit;
      }
      return $kits;
    }

    // RETORNO DE ARRAY DE TODOS OS KITS CADASTRADOS COM DETALHES
    public function getDetailedKits(){
      $kits = array();
      foreach(self::getAllKits() AS $kit){
        $kits[$kit['id']] = $kit;
        $kits[$kit['id']]['products'] = $this->db  ->where('kits.id',$kit['id'])
                                                  ->join('kits_products','kits_products.product_id = products.id','INNER')
                                                  ->join('kits','kits.id = kits_products.kit_id','INNER')
                                                  ->get('products')->result_array();
      }
      return $kits;
    }
    
    // RETORNO DE ARRAY COM DETALHES DE UM KIT ESPECÍFICO
    public function getKitData($id){
      $kit = $this->db  ->where('id',$id)
                        ->get('kits')
                        ->row_array();
      $kit['products'] = $this->db  ->where('kits.id',$kit['id'])
                                    ->join('kits_products','kits_products.product_id = products.id','INNER')
                                    ->join('kits','kits.id = kits_products.kit_id','INNER')
                                    ->get('products')->result_array();
      return $kit;
    }

	

}