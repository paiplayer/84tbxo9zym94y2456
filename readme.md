Para instalar a aplicação é necessário:

* Apache Server com PHP Versão 7.1 ou superior
* MySQL 5.6 ou MariaDB 10 ou superiores


Como instalar:

- Descompactar todo o projeto na raiz do Apache.
-- A aplicação está configurada para rodar na raiz.
-- Para rodar em uma subpasta altere a linha 2 do arquivo .htaccess indicando o caminho relativo da pasta onde a aplicação estiver rodando.
- Executar no Banco de Dados o arquivo que se encontra em /dump/mysql_dump.sql no banco de dados
- Configurar o arquivo /application/configs/database.php das linhas 9 à 12 com os dados de conexão com o Banco de dados


Observações:

- No exercício está descrito sobre a necessidade de exibição dos pedidos cadastrados apenas, não de cadastro de novos pedidos. 
-- Portanto não foi configurada nenhuma interface para este fim. 
- Tomei a liberdade de criar um método para exibir os kits completos.
-- Ele pode ser acessado no botão flutuante na página principal, o qual dispara a abertura de uma Modal que exibe os dados dos kits cadastrados.