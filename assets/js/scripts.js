/*
    collapsingControl É A VARIÁVEL DE CONTROLE QUE IMPEDE QUE UMA REQUISIÇÃO DE ATUALIZAÇÃO
    DO FRAME DE DETALHES DO PEDIDO SEJA ENVIADA QUANDO O COLLAPSE ESTÁ SENDO FECHADO
*/    
var collapsingControl = [];

// CARREGAMENTO DE DETALHES DO PEDIDO DENTRO DO COLLAPSE
$('.order-details').on('click',function(){
    var dataId = $(this).attr('data-id');
    if(collapsingControl[dataId] && collapsingControl[dataId]==1){
        collapsingControl[dataId] = 0;
    } else {
        collapsingControl[dataId] = 1;
        // APAGA O CONTEÚDO ATUAL DO CORPO QUE CONTÉM OS DETALHES
        $('#cardBody'+dataId).html('<center><img src="/assets/images/loading.gif" class="loading-image"><br><b>Carregando...</b></center>');
        $.ajax({
            type: "GET",
            url: '/main/orderDetails/'+dataId,
            dataType: 'html',
            success: function(result){
                $('#cardBody'+dataId).html(result);
            }
        });
    }
})

// CARREGAMENTO DE TELA COM LISTA DE KITS
$('#allKits').on('click',function(){
    $('#allKitsHolder').html('<center><img src="/assets/images/loading.gif" class="loading-image"><br><b>Carregando...</b></center>');
    $.ajax({
        type: "GET",
        url: '/main/kitsDetails/',
        dataType: 'html',
        success: function(result){
            $('#allKitsHolder').html(result);
        }
    });
})